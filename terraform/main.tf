provider "google" {
  project     = "training-243319"
  region      = "us-west1"
}

resource "random_id" "network_id" {
  byte_length = 8
}

resource "google_project_service" "compute" {
  service = "compute.googleapis.com"
}

# Create the network
module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 0.4.0"

  # Give the network a name and project
  project_id   = "${google_project_service.compute.project}"
  network_name = "my-custom-vpc-${random_id.network_id.hex}"

  subnets = [
    {
      # Creates your first subnet in us-west1 and defines a range for it
      subnet_name   = "my-first-subnet"
      subnet_ip     = "10.10.10.0/24"
      subnet_region = "us-west1"
    },
    {
      # Creates a dedicated subnet for my-seconde-subnet
      subnet_name   = "my-seconde-subnet"
      subnet_ip     = "10.10.20.0/24"
      subnet_region = "us-west1"
    },
  ]

  # Define secondary ranges for each of your subnets
  secondary_ranges = {
    my-first-subnet = []

    my-seconde-subnet = [
      {
        # Define a secondary range for Kubernetes pods to use
        range_name    = "my-seconde-subnet-range"
        ip_cidr_range = "192.168.64.0/24"
      },
    ]
  }
}

resource "random_id" "instance_id" {
  byte_length = 8
}

# Launch a VM on it
resource "google_compute_instance" "default" {

  name         = "vm-${random_id.instance_id.hex}"
  project      = "${google_project_service.compute.project}"
  machine_type = "n1-standard-16"
  zone         = "us-west1-a"

  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    subnetwork         = "${module.vpc.subnets_names[0]}"
    subnetwork_project = "${google_project_service.compute.project}"

    access_config {
      # Include this section to give the VM an external ip address
    }
  }

  # Web apache2 for test
  metadata_startup_script = "sudo apt-get update && sudo apt-get install apache2 -y && echo '<!doctype html><html><body><h1>Hello from Terraform on Google Cloud!</h1></body></html>' | sudo tee /var/www/html/index.html"

  # Apply the firewall rule to allow external IPs to ping this instance
  tags = ["firewall-custom-rules"]
}

# Allow traffic to the VM
resource "google_compute_firewall" "firewall-custom-rules" {
  name    = "default-ping"
  network = "${module.vpc.network_name}"
  project = "${google_project_service.compute.project}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "22", "443", "26257"]
  }

  allow {
    protocol = "udp"
    ports = ["1812", "1813"]
  }

  # Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["firewall-custom-rules"]
}

output "ip" {
  value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}


