provider "google-beta" {
  project = var.project
  region  = var.region
}

module "vmi" {
  source      = "../modules/vmi/"
  region = var.region
  zone = var.zone
  image = "jalove-image"
  instance_name = var.instance_name
  machine_type = var.machine_type
  name_prefix = var.name_prefix
  network = var.network
  instance_group_name = var.instance_group_name
  base_instance_name = var.base_instance_name
  target_size = 1
}






