variable "project" {
  description = "project name"
  type        = string
  default     = "training-243319"
}

variable "instance_name" {
  description = "instance_name"
  type        = string
  default     = "jalove-vm"
}

variable "region" {
  description = "region"
  type        = string
  default     = "us-central1"
}

variable "zone" {
  description = "zone"
  type        = string
  default     = "us-central1-a"
}

variable "image" {
  description = "image vm"
  type        = string
  default     = "jalove-image"
}

variable "machine_type" {
  description = "machine_type"
  type        = string
  default     = "n1-standard-1"
}

variable "target_size" {
  description = "target_size"
  type        = string
  default     = 1
}

variable "name_prefix" {
  description = "name_prefix"
  type        = string
  default     = "template-jalove"
}

variable "network" {
  description = "network"
  type        = string
  default     = "default"
}

variable "instance_group_name" {
  description = "instance_group_name"
  type        = string
  default     = "instance-group-tf-jalove"
}

variable "base_instance_name" {
  description = "base_instance_name"
  type        = string
  default     = "jalove"
}






