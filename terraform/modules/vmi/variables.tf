variable "project" {
  description = "project name"
  type        = string
  default     = ""
}

variable "instance_name" {
  description = "instance_name"
  type        = string
  default     = ""
}

variable "region" {
  description = "region"
  type        = string
  default     = ""
}

variable "zone" {
  description = "zone"
  type        = string
  default     = ""
}

variable "image" {
  description = "image vm"
  type        = string
  default     = ""
}

variable "machine_type" {
  description = "machine_type"
  type        = string
  default     = ""
}

variable "target_size" {
  description = "target_size"
  type        = string
  default     = ""
}

variable "name_prefix" {
  description = "name_prefix"
  type        = string
  default     = ""
}

variable "network" {
  description = "network"
  type        = string
  default     = ""
}

variable "instance_group_name" {
  description = "instance_group_name"
  type        = string
  default     = ""
}

variable "base_instance_name" {
  description = "base_instance_name"
  type        = string
  default     = ""
}






