resource "google_compute_instance_template" "template" {
  provider = google-beta

  name_prefix = var.name_prefix

  region = var.region

  lifecycle {
    create_before_destroy = true
  }

  network_interface {
    network = var.network
    access_config {
      // Ephemeral IP
      nat_ip = ""
    }
  }

  // machine_type         = "n1-standard-1"
  machine_type         = var.machine_type
  can_ip_forward       = false

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "TERMINATE"
  }
  // Create a new boot disk from an image
  disk {
    # source_image = "projects/ubuntu-os-cloud/global/images/ubuntu-1604-xenial-v20170516"
    source_image = var.image
    disk_type    = "pd-ssd"
    auto_delete  = true
    boot         = true
  }

  service_account {
    scopes = ["logging-write", "monitoring", "monitoring-write", "service-control", "service-management","storage-ro"]
  }
}

resource "google_compute_instance_group_manager" "group_manager" {
  provider = google-beta

  name = var.instance_group_name
  base_instance_name = var.base_instance_name
  target_size = var.target_size

  zone = var.zone
  version {
    name = "default"
    instance_template = google_compute_instance_template.template.self_link
  }
}


