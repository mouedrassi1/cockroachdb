export GOOGLE_CLOUD_KEYFILE_JSON=~/packer/key.json


$ gcloud config list
[compute]
region = us-central1
zone = us-central1-f
[core]
account = mohamedo@datavalet.com
disable_usage_reporting = True
project = training-243319
