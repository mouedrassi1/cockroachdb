## Configuring instances and instance groups
gcloud compute instances create vm1central1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

gcloud compute instances create vm2central1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

gcloud compute instances create vm1east1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-east1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

gcloud compute instances create vm2east1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-east1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

