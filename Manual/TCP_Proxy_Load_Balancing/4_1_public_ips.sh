gcloud beta compute forwarding-rules create lb-ipv4-forwarding-rule \
    --global \
    --target-tcp-proxy lb-target-proxy \
    --address 35.244.220.233 \
    --ports 110

gcloud beta compute forwarding-rules create lb-ipv6-forwarding-rule \
    --global \
    --target-tcp-proxy lb-target-proxy \
    --address 2600:1901:0:ff9c:: \
    --ports 110

# 35.244.220.233:110
