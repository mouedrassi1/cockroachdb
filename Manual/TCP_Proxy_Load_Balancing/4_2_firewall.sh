## Creating a firewall rule for the TCP load balancer
gcloud compute firewall-rules create cockroachadmin \
     --source-ranges 0.0.0.0/0 \
     --target-tags cockroachdb \
     --allow tcp:8080,tcp:26257