## Creating an instance group for each zone and adding instances
gcloud compute instance-groups unmanaged create gp1 \
    --zone us-central1-b

gcloud compute instance-groups set-named-ports gp1 \
    --named-ports tcp:8080,tcp:26257 \
    --zone us-central1-b

gcloud compute instance-groups unmanaged add-instances gp1 \
    --instances vm1central1,vm2central1 \
    --zone us-central1-b

gcloud compute instance-groups unmanaged create gp2 \
    --zone us-east1-b

gcloud compute instance-groups set-named-ports gp2 \
    --named-ports tcp:8080,tcp:26257 \
    --zone us-east1-b

gcloud compute instance-groups unmanaged add-instances gp2 \
    --instances vm1east1,vm2east1 \
    --zone us-east1-b
