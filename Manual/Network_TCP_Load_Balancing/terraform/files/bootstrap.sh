#! /bin/bash
sudo apt-get update
wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/
cockroach start --insecure --join=vm0,vm1,vm2 --background
cockroach init --insecure --host=vm0