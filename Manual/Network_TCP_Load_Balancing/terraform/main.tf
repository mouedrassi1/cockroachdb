//https://stackoverflow.com/questions/48253865/how-to-load-balance-google-compute-instance-using-terraform/48267289
provider "google" {
  project     = "training-243319"
  region      = "us-central1"
}

data "template_file" "metadata_startup_script" {
  template = file("${path.module}/files/bootstrap.sh")
}

resource "google_compute_instance" "vm" {
  count = 3
  name  = "vm${count.index}"

  machine_type = "n1-standard-1"
  zone         = "us-central1-a"
  tags = ["cockroachdb"]
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  // Local SSD disk
  scratch_disk {
    interface = "SCSI"
  }
  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }
  }
  metadata_startup_script = data.template_file.metadata_startup_script.rendered
}

resource "google_compute_target_pool" "default" {
  name = "cockroachdb-pool"

  instances = [
    //    google_compute_instance.vm.*.self_link[0],
    //    google_compute_instance.vm2.*.self_link[0],
    //    google_compute_instance.vm3.*.self_link[0],
    "us-central1-a/vm0",
    "us-central1-a/vm1",
    "us-central1-a/vm2",
  ]

  health_checks = [
    google_compute_http_health_check.default.name,
  ]

}

resource "google_compute_http_health_check" "default" {
  name               = "cockroachdb-health-check"
  request_path       = "/health?ready=1"
  check_interval_sec = 5
  timeout_sec        = 5
  port               = "8080"
}

resource "google_compute_firewall" "default" {
  name    = "cockroach-firewall"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "26257"]
  }

  target_tags = ["cockroachdb"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_address" "ip4" {
  name = "cockroachdb-ip4"
}

resource "google_compute_forwarding_rule" "rule-8080" {
  name                  = "cockroachdb-rule-8080"
  region                = "us-central1"
  load_balancing_scheme = "EXTERNAL"
  target                = google_compute_target_pool.default.self_link
  port_range            = "8080"
  ip_address            = google_compute_address.ip4.address
}

resource "google_compute_forwarding_rule" "rule-26257" {
  name                  = "cockroachdb-rule-26257"
  region                = "us-central1"
  load_balancing_scheme = "EXTERNAL"
  target                = google_compute_target_pool.default.self_link
  port_range            = "26257"
  ip_address            = google_compute_address.ip4.address
}

