## Configuring instances and instance groups
gcloud compute instances create vm1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

gcloud compute instances create vm2 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

gcloud compute instances create vm3 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
      sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/'

## Creating a firewall rule for the TCP load balancer
gcloud compute firewall-rules create cockroach-firewall \
   --target-tags=cockroachdb \
   --action=allow \
   --rules=tcp:8080,tcp:26257

## public ip
gcloud compute addresses create cockroachdb-ip4 \
    --region us-central1

gcloud compute health-checks create http cockroachdb-health-check \
  --port=8080 \
  --request-path=/health\?ready=1 \
  --proxy-header=NONE \
  --check-interval=5 \
  --timeout=5 \
  --unhealthy-threshold=2 \
  --healthy-threshold=2
#
#gcloud compute target-pools create cockroachdb-pool \
#    --region=us-central1 \
#    --http-health-check=cockroachdb-health-check

gcloud compute target-pools add-instances cockroachdb-pool \
    --instances vm1,vm2,vm3 \
    --instances-zone us-central1-b

gcloud compute forwarding-rules create cockroachdb-rule-26257 \
    --region us-central1 \
    --ports 26257 \
    --address cockroachdb-ip4 \
    --target-pool cockroachdb-pool

gcloud compute forwarding-rules create cockroachdb-rule-8080 \
    --region us-central1 \
    --ports 8080 \
    --address cockroachdb-ip4 \
    --target-pool cockroachdb-pool

gcloud compute forwarding-rules describe cockroachdb-rule-26257 --region us-central1
gcloud compute forwarding-rules describe cockroachdb-rule-8080 --region us-central1



