provider "google" {
  //  credentials = "${file("key.json")}"
  project = "training-243319"
}

provider "google-beta" {
  //  credentials = "${file("key.json")}"
  project = "training-243319"
}

data "google_compute_network" "default" {
  name = "default"
}

resource "google_compute_firewall" "web_access" {
  name = "web-access"
  allow {
    protocol = "tcp"
    ports = [ 80 ]
  }
  network = "${data.google_compute_network.default.name}"
  source_ranges = [ "0.0.0.0/0" ]
}

resource "google_compute_instance_template" "web" {
  name_prefix = "web-"
  machine_type = "f1-micro"
  disk {
    source_image = "debian-cloud/debian-9"
  }
  network_interface {
    network = "${data.google_compute_network.default.name}"
    access_config {
    }
  }
  metadata_startup_script = "apt-get update ; apt-get install -y nginx"
}

resource "google_compute_health_check" "web_check" {
  name = "web-check"
  check_interval_sec = 5
  timeout_sec = 5
  healthy_threshold = 2
  unhealthy_threshold = 2
  http_health_check {
    request_path = "/"
    port = "80"
  }
}

resource "google_compute_instance_group_manager" "web_group" {

  provider = "google-beta"
  name = "web-group"
  base_instance_name = "web"
  target_size = 1

  version {
    name = "default"
    instance_template = "${google_compute_instance_template.web.self_link}"
  }

  zone = "us-central1-f"

  auto_healing_policies {
    health_check = "${google_compute_health_check.web_check.self_link}"
    initial_delay_sec = 30
  }

  lifecycle {
    ignore_changes = [
      "version.0.name"
    ]
  }

}