wget -qO- https://binaries.cockroachdb.com/cockroach-v19.2.1.linux-amd64.tgz | tar  xvz
sudo cp -i cockroach-v19.2.1.linux-amd64/cockroach /usr/local/bin/
cockroach start \
--insecure \
--join=vm1central1,vm2central1,vm1east1,vm2east1 \
--background

cockroach init --insecure --host=10.128.0.58