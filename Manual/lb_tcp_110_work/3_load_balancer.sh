## Configuring the load balancer
gcloud compute health-checks create tcp httphealth-check --port 110

gcloud compute backend-services create proxy-lb \
    --global \
    --protocol TCP \
    --health-checks httphealth-check \
    --timeout 5m \
    --port-name tcp110

gcloud compute backend-services add-backend proxy-lb \
    --global \
    --instance-group gp1 \
    --instance-group-zone us-central1-b \
    --balancing-mode UTILIZATION \
    --max-utilization 0.8

gcloud compute backend-services add-backend proxy-lb \
    --global \
    --instance-group gp2 \
    --instance-group-zone us-east1-b \
    --balancing-mode UTILIZATION \
    --max-utilization 0.8

gcloud compute target-tcp-proxies create lb-target-proxy \
    --backend-service proxy-lb \
    --proxy-header NONE

gcloud compute addresses create tcp-lb-static-ipv4 \
    --ip-version=IPV4 \
    --global

gcloud compute addresses create tcp-lb-static-ipv6 \
    --ip-version=IPV6 \
    --global

