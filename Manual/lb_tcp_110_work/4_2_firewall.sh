## Creating a firewall rule for the TCP load balancer
gcloud compute firewall-rules create cockroachadmin \
     --source-ranges 130.211.0.0/22,35.191.0.0/16 \
     --target-tags cockroachdb \
     --allow tcp:110