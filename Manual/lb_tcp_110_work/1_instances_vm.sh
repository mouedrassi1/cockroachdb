## Configuring instances and instance groups
gcloud compute instances create vm1central1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      sudo apt-get install apache2 -y
      sudo sed -i "/Listen 80/c\Listen 110" /etc/apache2/ports.conf
      sudo sed -i "/\/c\\" /etc/apache2/sites-enabled/000-default.conf
      sudo service apache2 restart
      echo "<!doctype html><html><body><h1>vm1central1</h1></body></html>" | tee /var/www/html/index.html
      EOF'

gcloud compute instances create vm2central1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-central1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      sudo apt-get install apache2 -y
      sudo sed -i "/Listen 80/c\Listen 110" /etc/apache2/ports.conf
      sudo sed -i "/\/c\\" /etc/apache2/sites-enabled/000-default.conf
      sudo service apache2 restart
      echo "<!doctype html><html><body><h1>vm2central1</h1></body></html>" | tee /var/www/html/index.html
      EOF'

gcloud compute instances create vm1east1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-east1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      sudo apt-get install apache2 -y
      sudo sed -i "/Listen 80/c\Listen 110" /etc/apache2/ports.conf
      sudo sed -i "/\/c\\" /etc/apache2/sites-enabled/000-default.conf
      sudo service apache2 restart
      echo "<!doctype html><html><body><h1>vm1east1</h1></body></html>" | tee /var/www/html/index.html
      EOF'

gcloud compute instances create vm2east1 \
    --image-family debian-9 \
    --image-project debian-cloud \
    --tags cockroachdb \
    --zone us-east1-b \
    --metadata startup-script='#! /bin/bash
      sudo apt-get update
      sudo apt-get install apache2 -y
      sudo sed -i "/Listen 80/c\Listen 110" /etc/apache2/ports.conf
      sudo sed -i "/\/c\\" /etc/apache2/sites-enabled/000-default.conf
      sudo service apache2 restart
      echo "<!doctype html><html><body><h1>vm2east1</h1></body></html>" | tee /var/www/html/index.html
      EOF'

