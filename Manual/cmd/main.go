//https://medium.com/@irshadhasmat/golang-getting-started-with-cockroachdb-with-go-language-21c67ab569b0
package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	// Connect to the "company_db" database.
	// Create database company_db;
	// use company_db;
	// CREATE TABLE "tbl_employee" (
	//	"employee_id" SERIAL,
	//	"full_name" STRING(100),
	//	"department" STRING(50),
	//	"designation" STRING(50),
	//	"created_at" TIMESTAMPTZ,
	//	"updated_at" TIMESTAMPTZ,
	//	PRIMARY KEY ("employee_id")
	// );

	db, err := sql.Open("postgres", "postgresql://root@34.66.223.108:26257/company_db?sslmode=disable")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	// Insert a row into the "tbl_employee" table.
	if _, err := db.Exec(
		`INSERT INTO tbl_employee (full_name, department, designation, created_at, updated_at) 
		VALUES ('Ouedrassi', 'IT', 'Developer', NOW(), NOW());`); err != nil {
		log.Fatal(err)
	}

	// Select Statement.
	rows, err := db.Query("select employee_id, full_name FROM tbl_employee;")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var employeeId int64
		var fullName string
		if err := rows.Scan(&employeeId, &fullName); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Employee Id : %d \t Employee Name : %s\n", employeeId, fullName)
	}
}


