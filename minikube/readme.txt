################################## Orchestrate a Local Cluster with Kubernetes ##################################

minikube start

curl -O https://raw.githubusercontent.com/cockroachdb/cockroach/master/cloud/kubernetes/cockroachdb-statefulset-secure.yaml
curl -O https://raw.githubusercontent.com/cockroachdb/cockroach/master/cloud/kubernetes/cluster-init-secure.yaml
curl -O https://raw.githubusercontent.com/cockroachdb/cockroach/master/cloud/kubernetes/client-secure.yaml

** deploy cockroachdb-statefulset-secure.yaml
kubectl create -f cockroachdb-statefulset-secure.yaml

kubectl get csr
kubectl describe csr default.node.cockroachdb-0

kubectl certificate approve default.node.cockroachdb-0
kubectl certificate approve default.node.cockroachdb-1
kubectl certificate approve default.node.cockroachdb-2

kubectl get pods
kubectl get persistentvolumes

** Use our cluster-init-secure.yaml file to perform a one-time initialization that joins the CockroachDB nodes into a single cluster
kubectl create -f https://raw.githubusercontent.com/cockroachdb/cockroach/master/cloud/kubernetes/cluster-init-secure.yaml
kubectl certificate approve default.client.root
kubectl get job cluster-init-secure
kubectl get pods

** use our client-secure.yaml file to launch a pod and keep it running indefinitely
kubectl create -f https://raw.githubusercontent.com/cockroachdb/cockroach/master/cloud/kubernetes/client-secure.yaml

** Get a shell into the pod and start the CockroachDB built-in SQL client
kubectl exec -it cockroachdb-client-secure -- ./cockroach sql --certs-dir=/cockroach-certs --host=cockroachdb-public


** Run some basic CockroachDB SQL statements:
CREATE DATABASE bank;
CREATE TABLE bank.accounts (id INT PRIMARY KEY, balance DECIMAL);
INSERT INTO bank.accounts VALUES (1, 1000.50);
SELECT * FROM bank.accounts;

** Create a user with a password
CREATE USER roach WITH PASSWORD 'Q7gc8rEdS';

** Access the Admin UI
kubectl port-forward cockroachdb-0 8080
https://localhost:8080